package com.our.base.easylog.config.es;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * auth: xiaowu
 * es 配置信息
 */
@Data
@ConfigurationProperties(prefix ="easy-log.es")
public class EsProperties {
    /**
     * 集群地址
     */
    private String[] host;

    /**
     * 用户名
     */
    private String user;

    /**
     * 密码
     */
    private String password;

    /**
     * 索引名
     */
    private String indexName;

    /**
     * 索引配置模板
     */
    private String template;

    /**
     * 管道名
     */
    private String[] pipeline;

    /**
     * 管道配置文件地址
     */
    private String pipelineUrl;
}
