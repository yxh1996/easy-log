package com.our.base.easylog;

import com.our.base.easylog.entity.LogInfo;

/**
 * 日志推送接口
 * @author Alice
 * @date 2023/11/29
 **/
public interface IPushLog {
    /**
     * 推送日志
     * @param logInfo 日志信息
     */
    void pushLog(LogInfo logInfo);
}
