package com.our.base.easylog;

import com.our.base.easylog.config.EasyLogProperties;
import com.our.base.easylog.es.EsPushLogImpl;
import com.our.base.easylog.config.es.EsProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alice
 */
@Configuration
@EnableConfigurationProperties({EsProperties.class, EasyLogProperties.class})
@ConditionalOnProperty(prefix = "easy-log", name = "enable", matchIfMissing = true)
public class EasyLogAutoConfigure {
    @Autowired
    private EasyLogProperties easyLogProperties;

    @Bean
    @ConditionalOnProperty(prefix = "easy-log",name = "storage",havingValue = "es")
    @ConditionalOnMissingBean(IPushLog.class)
    public IPushLog pushLog() {
        return new EsPushLogImpl(easyLogProperties.getEsProperties());

    }

}
