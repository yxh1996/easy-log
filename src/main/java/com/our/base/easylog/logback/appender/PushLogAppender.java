package com.our.base.easylog.logback.appender;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.Layout;
import ch.qos.logback.core.LogbackException;
import ch.qos.logback.core.filter.Filter;
import com.our.base.easylog.IPushLog;
import com.our.base.easylog.entity.LogInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** 日志推送Appender
 * @author Alice
 * @date 2023/11/29
 **/
@Component
public class PushLogAppender extends AppenderBase<ILoggingEvent> {
    @Autowired
    IPushLog pushLog;

    @Override
    protected void append(ILoggingEvent eventLog) {
        //拿到日志
        String log = eventLog.getFormattedMessage();
        //读取配置(根据配置的MDC信息做分词标签)
            eventLog.getMDCPropertyMap();
        LogInfo logInfo=new LogInfo();
            pushLog.pushLog(logInfo);



    }
}

