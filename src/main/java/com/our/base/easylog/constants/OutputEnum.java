package com.our.base.easylog.constants;

/**
 * @author wujun
 * @date 2023/11/30
 * @description 日志输出端
 */
public enum OutputEnum {
    /**
     * elasticsearch
     */

    ES("elasticsearch", "es"),
    /**
     * mysql
     */
    MYSQL("mysql", "mysql");

    public final String type;
    public final String msg;

    OutputEnum(String type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public static String getType(String type) {
        OutputEnum[] values = OutputEnum.values();
        for (OutputEnum value : values) {
            if (value.type.equals(type)){
                return value.type;
            }
        }
        return "";
    }
}
