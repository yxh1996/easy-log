package com.our.base.easylog.config;

import com.our.base.easylog.config.es.EsProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 主配置文件
 * @author Alice
 * @date 2023/11/29
 **/
@Data
@ConfigurationProperties(prefix = "easy-log")
public class EasyLogProperties {
    private boolean enable;
    private String storage;
    private EsProperties esProperties;


}
