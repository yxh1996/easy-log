package com.our.base.easylog.es;

import com.our.base.easylog.IPushLog;
import com.our.base.easylog.entity.LogInfo;
import com.our.base.easylog.config.es.EsProperties;

/**
 * es日志推送实现
 * @author Alice
 * @date 2023/11/29
 **/

public class EsPushLogImpl implements IPushLog {
    private  final EsProperties esProperties;
    public EsPushLogImpl(EsProperties esProperties){
        this.esProperties=esProperties;

    }
    @Override
    public void pushLog(LogInfo logInfo) {

    }
}
