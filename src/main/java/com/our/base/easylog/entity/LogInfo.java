package com.our.base.easylog.entity;

import lombok.Data;

/** 日志信息类（后续与日志相关的字段全部在这里获取）
 * @author Alice
 * @date 2023/11/29
 **/
@Data
public class LogInfo {
    /**
     * 日志主信息
     */
    private String log;


}
